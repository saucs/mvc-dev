﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication9.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Olustur()
        {
            return View();
        }
        
        public ActionResult TurYazar()
        {
            return View();
        }

        public ActionResult TurHakkinda()
        {
            return View();
        }
        public ActionResult Hakkimizda()
        {
            return View();
        }
        
        public ActionResult Iletisim()
        {
            return View();
        }

        
        //<---------------TÜRLER------------------>

        public ActionResult Makale()
        {
            return View();
        }
        public ActionResult Deneme()
        {
            return View();
        }
        public ActionResult Elestiri()
        {
            return View();
        }
        public ActionResult Masal()
        {
            return View();
        }
        public ActionResult Fikra()
        {
            return View();
        }
        public ActionResult GeziYazisi()
        {
            return View();
        }

    }
}