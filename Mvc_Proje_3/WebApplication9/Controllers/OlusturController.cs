﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using WebApplication9.DAL;
using WebApplication9.Models;

namespace WebApplication9.Controllers
{
    public class OlusturController : Controller
    {
        private KayitContext db = new KayitContext();

        // GET: Olustur
        public ActionResult Index()
        {
            var olusturulanlar = db.Olusturulanlar.Include(o => o.Kategori);
            return View(olusturulanlar.ToList());
        }

        // GET: Olustur/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Olustur olustur = db.Olusturulanlar.Find(id);
            if (olustur == null)
            {
                return HttpNotFound();
            }
            return View(olustur);
        }

        // GET: Olustur/Create
        public ActionResult Create()
        {
            ViewBag.KategoriId = new SelectList(db.Kategoriler, "Id", "Ad");
            return View();
        }

        // POST: Olustur/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,KategoriId,Baslik,Dil,Aciklama,foto1,Yazi")] Olustur olustur ,HttpPostedFileBase file)
        {
            using (KayitContext context = new KayitContext())
            {
                Olustur _slide = new Olustur();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    _slide.foto1 = memoryStream.ToArray();
                }
                _slide.Baslik = olustur.Baslik;
                _slide.KategoriId = olustur.KategoriId;
                _slide.Dil = olustur.Dil;
                _slide.Aciklama = olustur.Aciklama;
                _slide.Yazi = olustur.Yazi;
                db.Olusturulanlar.Add(_slide);
                db.SaveChanges();
            }

            ViewBag.KategoriId = new SelectList(db.Kategoriler, "Id", "Ad", olustur.KategoriId);
            return RedirectToAction("Index");
        }

        // GET: Olustur/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Olustur olustur = db.Olusturulanlar.Find(id);
            if (olustur == null)
            {
                return HttpNotFound();
            }
            ViewBag.KategoriId = new SelectList(db.Kategoriler, "Id", "Ad", olustur.KategoriId);
            return View(olustur);
        }

        // POST: Olustur/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,KategoriId,Baslik,Dil,Aciklama,foto1,Yazi")] Olustur olustur ,HttpPostedFileBase file)
        {
            using (KayitContext context = new KayitContext())
            {
                Olustur _slide = new Olustur();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    _slide.foto1 = memoryStream.ToArray();
                }
                _slide.Baslik = olustur.Baslik;
                _slide.KategoriId = olustur.KategoriId;
                _slide.Dil = olustur.Dil;
                _slide.Aciklama = olustur.Aciklama;
                _slide.Yazi = olustur.Yazi;
                db.Olusturulanlar.Add(_slide);
                db.SaveChanges();
            }

            ViewBag.KategoriId = new SelectList(db.Kategoriler, "Id", "Ad", olustur.KategoriId);
            return RedirectToAction("Index");
        }

        // GET: Olustur/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Olustur olustur = db.Olusturulanlar.Find(id);
            if (olustur == null)
            {
                return HttpNotFound();
            }
            return View(olustur);
        }

        // POST: Olustur/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Olustur olustur = db.Olusturulanlar.Find(id);
            db.Olusturulanlar.Remove(olustur);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public ActionResult Kategori(int id)
        {
            string kategoriAdi = (from k in db.Kategoriler
                                  where k.Id == id
                                  select k.Ad).FirstOrDefault();
            ViewBag.Title = kategoriAdi + "Kategorisindeki Ürünler";
            ViewBag.Id = id;
            List<Olustur> olusturulanlar = (from u in db.Olusturulanlar
                                            where u.KategoriId == id
                                            select u).ToList();
            return View(olusturulanlar);
        }
        [HttpPost]
        public ActionResult sayfa(string itemId)
        {
            if (itemId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Olustur bSayfa = db.Olusturulanlar.Find(Convert.ToInt32(itemId));
            if (bSayfa == null)
            {
                return HttpNotFound();
            }
            return View(bSayfa);
        }

        public ActionResult Index1()
        {
            var res = db.Olusturulanlar.OrderByDescending(x => x.Id);
            var olusturulanlar = db.Olusturulanlar.Include(o => o.Kategori);
            return View(res.ToList());
        }
        

    }
}
