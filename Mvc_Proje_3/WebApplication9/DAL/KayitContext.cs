﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using WebApplication9.Models;

namespace WebApplication9.DAL
{
    public class KayitContext:DbContext
    {
        public KayitContext() : base("KayitVeritabani") { }
        public DbSet<Kategori> Kategoriler { get; set; }
        public DbSet<Olustur> Olusturulanlar { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}