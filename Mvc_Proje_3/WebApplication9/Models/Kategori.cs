﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication9.Models
{
    public class Kategori
    {
        public int Id { get; set; }
        public string Ad { get; set; }
        public virtual ICollection<Olustur> Olusturulanlar { get; set; }
    }
}