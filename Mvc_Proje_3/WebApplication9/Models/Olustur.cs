﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace WebApplication9.Models
{
    public class Olustur
    {
        public int Id { get; set; }
        [DisplayName("Tür")]
        public int  KategoriId { get; set; }
        [DisplayName("Başlık")]
        public string  Baslik { get; set; }
        public string Dil { get; set; }
        [DisplayName("Açıklama")]
        public string Aciklama { get; set; }
        [DisplayName("Fotoğraf")]
        public byte[] foto1 { get; set; }
        [DisplayName("Metin")]
        public string Yazi { get; set; }
        public virtual Kategori Kategori { get; set; }

    }
}